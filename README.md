# Go Pupper Go - PyGame

This game was created using `python` and `Pygame`.

All sprites, images and audio are original content.

## Background

> We are the puppers. Whenever you humans see us on the road you try to pet us, kiss us and hug us. However, all we want is the holy shiny gold bone. Legend says that we have to cross blue water monsters, huge stones and portals that suck you into a world full of firework and thunder sounds. In the present world, we even have to escape the love you humans shower on us. We certainly want to avoid all these. Two of our dogs try to make this perilous journey every day, the dog that lasts the longest gets to go to bone-heaven.
>
> -- <cite> Good Boi IV Jr. </cite>

## Controls

| Dog Going Up | Button              |
|--------------|---------------------|
| Move Up    | <kbd>up</kbd>     |
| Move Down   | <kbd>down</kbd>    |
| Move Left      | <kbd>left</kbd>       |
| Move Right    | <kbd>right</kbd>     |

| Dog Going Down | Button              |
|--------------|---------------------|
| Move Up    | <kbd>w</kbd>     |
| Move Down   | <kbd>s</kbd>    |
| Move Left      | <kbd>a</kbd>       |
| Move Right    | <kbd>d</kbd>     |

Press 'q' to quit the game

## Requirements

Python's Pygame is required to run this game.


## For Ubuntu/Debian

```bash
$ sudo apt-get install python3-pygame
```
## Running The Game

Clone the repository and run the command
```bash
$ python3 game.py
```
