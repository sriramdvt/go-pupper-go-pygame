heartspeed1 = 5
heartspeed2 = 6

white = (255, 255, 255)
fontstyle = 'freesansbold.ttf'

smallfontsize = 10
largefontsize = 50

dogupwins = 'Dog Going Up Wins'
score = 'Score: '
dogdownwins = 'Dog Going Down Wins'
tiemessage = "It's a Tie Puppers"
