import pygame
from random import seed, random
from datetime import datetime
import config
seed(datetime.now())

pygame.init()

""" Game and Screen Settings """
windowheight = 980
windowwidth = 1000
win = pygame.display.set_mode((windowwidth, windowheight))

""" Importing All Custom Sprites and Background Image """
heart = [pygame.image.load('Images/HeartBig.png'),
         pygame.image.load('Images/HeartSmall.png')]
blueMonster = pygame.image.load('Images/BlueDragon.png')
bone = pygame.image.load('Images/Bone.png')
blackHole = pygame.image.load('Images/BlackHole.png')
stone = pygame.image.load('Images/Rock.png')
dog = [pygame.image.load('Images/DogFront.png'),
       pygame.image.load('Images/DogBack.png')]
bg = pygame.image.load('Images/BigBackground.png')

""" Importing the Custom Sound Effects """
backgroundmusic = pygame.mixer.music.load('BackgroundMusic.wav')
yaay = pygame.mixer.Sound('Yaay.wav')
oof = pygame.mixer.Sound('Oof.wav')
pygame.mixer.music.play(-1)


pygame.display.set_caption("Go Pupper Go")
clock = pygame.time.Clock()

font = pygame.font.Font(config.fontstyle, config.smallfontsize)
fontbig = pygame.font.Font(config.fontstyle, config.largefontsize)

""" Game Settings End  """


""" Classes """
""" Class Player is the main player """


class player(object):
    def __init__(self, x, y, width, height, vel, img):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = vel
        self.hitbox = (self.x + 20, self.y, 28, 55)
        self.isalive = True
        self.img = img
        self.crossed = False
        self.totalalive = True
        self.score = 0
        self.first = 0
        self.second = 0
        self.third = 0
        self.fourth = 0
        self.fifth = 0
        self.sixth = 0
        self.seventh = 0
        self.eighth = 0
        self.ninth = 0
        self.tenth = 0
        self.eleventh = 0
        self.timescore = 3000

    def draw(self, win):
        win.blit(dog[self.img], (self.x, self.y))
        self.hitbox = (self.x + 17, self.y + 2, 31, 55)

    """ Function to be executed when player hits an obstacle """

    def obstaclehit(self):
        self.totalalive = False
        oof.play()
        self.hit()

    """ Function to be executed when players have to switch """

    def hit(self):
        if dogup.isalive:
            if dogdown.totalalive:
                dogup.isalive = False
                dogdown.isalive = True
                dogdown.x = 500
                dogdown.y = 0
                bone1.isalive = False
                bone2.isalive = True
            else:
                dogup.x = 500
                dogup.y = 980-64
                nextlevel()
        else:
            if dogup.totalalive:
                dogup.isalive = True
                dogdown.isalive = False
                dogup.x = 500
                dogup.y = 980-64
                bone1.isalive = True
                bone2.isalive = False
            else:
                dogdown.x = 500
                dogdown.y = 0
                nextlevel()

    """ Function to be executed when player touches the bone """

    def bonetouch(self):
        yaay.play()
        self.crossed = True
        if dogup.isalive:
            if dogdown.crossed:
                nextlevel()
            else:
                pass
        else:
            if dogup.crossed:
                nextlevel()
            else:
                pass
        self.hit()


""" All Fixed Obstacles are in the class Obstacle """


class obstacle(object):

    def __init__(self, x, y, imagesource):
        self.x = x
        self.y = y
        self.imagesource = imagesource
        self.hitbox = (self.x + 10, self.y, 28, 60)

    def draw(self, win):
        win.blit(self.imagesource, (self.x, self.y))
        self.hitbox = (self.x + 10, self.y, 50, 57)


""" The Bones belong to the class Target """


class target(object):

    def __init__(self, x, y, imagesource, isalive):
        self.x = x
        self.y = y
        self.imagesource = imagesource
        self.hitbox = (self.x, self.y + 35, 65, 30)
        self.isalive = isalive

    def draw(self, win):
        if self.isalive:
            win.blit(self.imagesource, (self.x, self.y))
            self.hitbox = (self.x, self.y + 35, 65, 30)


""" The Moving Obstacles Belong to the class HeartObstacle """


class heartobstacle(object):

    def __init__(self, x, y, vel, count, direction):
        self.x = x
        self.y = y
        self.vel = vel
        self.count = count
        self.direction = direction
        self.hitbox = (self.x + 17, self.y, 40, 55)

    def draw(self, win, count):
        win.blit(heart[self.count//10], (self.x, self.y))
        self.hitbox = (self.x + 7, self.y + 20, 47, 45)


""" Classes End """

""" Functions """
""" Function to Increase the Velocities of the Moving Obstacles"""
""" And to reset the fixed obstacle scoring system """


def nextlevel():
    if dogup.crossed:
        dogup.score += int(dogup.timescore/40)
    if dogdown.crossed:
        dogdown.score += int(dogdown.timescore/40)
    dogup.timescore = 3000
    dogdown.timescore = 3000
    dogup.crossed = False
    dogdown.crossed = False
    heart1.vel += 2
    heart2.vel += 2
    heart3.vel += 2
    heart4.vel += 2
    heart5.vel += 2
    heart6.vel += 2
    heart7.vel += 2
    heart8.vel += 2
    dogup.first = 0
    dogup.second = 0
    dogup.third = 0
    dogup.fourth = 0
    dogup.fifth = 0
    dogup.sixth = 0
    dogup.seventh = 0
    dogup.eighth = 0
    dogup.ninth = 0
    dogup.tenth = 0
    dogup.eleventh = 0
    dogdown.first = 0
    dogdown.second = 0
    dogdown.third = 0
    dogdown.fourth = 0
    dogdown.fifth = 0
    dogdown.sixth = 0
    dogdown.seventh = 0
    dogdown.eighth = 0
    dogdown.ninth = 0
    dogdown.tenth = 0
    dogdown.eleventh = 0


""" Function to redraw the entire game window"""
""" during every run of the game's main loop """


def drawGameWindow():
    win.blit(bg, (0, 0))
    if dogup.isalive:
        dogup.draw(win)
    else:
        dogdown.draw(win)
    heart1.draw(win, heart1.count)
    heart2.draw(win, heart1.count)
    heart3.draw(win, heart3.count)
    heart4.draw(win, heart4.count)
    heart5.draw(win, heart5.count)
    heart6.draw(win, heart6.count)
    heart7.draw(win, heart7.count)
    heart8.draw(win, heart8.count)
    bluemonster1.draw(win)
    bluemonster2.draw(win)
    bluemonster3.draw(win)
    blackhole1.draw(win)
    blackhole2.draw(win)
    blackhole3.draw(win)
    rock1.draw(win)
    rock2.draw(win)
    rock3.draw(win)
    bone1.draw(win)
    bone2.draw(win)
    score1 = font.render(
        'Dogup Score: ' + str(dogup.score), 1, config.white)
    win.blit(score1, (5, 10))
    score2 = font.render('Dogdown Score: ' +
                         str(dogdown.score), 1, config.white)
    win.blit(score2, (5, 20))
    timescore1 = font.render('Dogup Time Bonus: ' +
                             str(int(dogup.timescore/40)), 1, config.white)
    if dogup.isalive:
        win.blit(timescore1, (850, 10))
    timescore2 = font.render('Dogdown Time Bonus: ' +
                             str(int(dogdown.timescore/40)), 1, config.white)
    if dogdown.isalive:
        win.blit(timescore2, (850, 20))
    pygame.display.update()


""" Functions End """


""" Initialising Objects """
dogup = player(500, 980-64, 64, 64, 5, 1)
dogdown = player(500, 0, 64, 64, 5, 0)

dogdown.isalive = False
dogup.crossed = False
dogdown.crossed = False
bone1 = target(500, 0, bone, True)
bone2 = target(500, 980-64, bone, False)
""" Initialising Moving and Fixed Obstacles at Random Horizontal Positions """
heart1 = heartobstacle((random()*10000) % 700, 80, config.heartspeed2, 5, True)
heart2 = heartobstacle((random()*10000) %
                       700, 240, config.heartspeed2, 2, False)
heart3 = heartobstacle((random()*10000) %
                       700, 380, config.heartspeed1, 9, True)
heart4 = heartobstacle((random()*10000) %
                       700, 530, config.heartspeed2, 7, False)
heart5 = heartobstacle((random()*10000) %
                       700, 660, config.heartspeed2, 5, False)
heart6 = heartobstacle((random()*10000) %
                       700, 810, config.heartspeed2, 2, True)
heart7 = heartobstacle((random()*10000) %
                       700, 380, config.heartspeed1, 9, False)
heart8 = heartobstacle((random()*10000) %
                       700, 530, config.heartspeed2, 7, True)
bluemonster1 = obstacle(0 + ((random() * 1000) % 500), 175, blueMonster)
bluemonster2 = obstacle(700 + ((random() * 1000) % 100), 459, blueMonster)
bluemonster3 = obstacle(300 + ((random() * 1000) % 100), 605, blueMonster)
blackhole1 = obstacle(500 + ((random() * 1000) % 100), 175, blackHole)
blackhole2 = obstacle(800 + ((random() * 1000) % 100), 600, blackHole)
blackhole3 = obstacle(50 + ((random() * 1000) % 100), 459, blackHole)
rock1 = obstacle(700 + ((random() * 1000) % 100), 320, stone)
rock2 = obstacle(350 + ((random() * 1000) % 100), 459, stone)
rock3 = obstacle(500 + ((random() * 1000) % 100), 750, stone)


""" Main Loop of the Game """
run = True
while run:
    clock.tick(30)

    """ Pressing the X button at the top-right will exit the game """
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    keys = pygame.key.get_pressed()

    """ Controls for Dog Going Up are the Arrow Keys """
    if(dogup.isalive):
        """ Pressing Q will quit the game """
        if keys[pygame.K_q]:
            run = False
        if keys[pygame.K_LEFT] and dogup.x > dogup.vel:
            dogup.x -= dogup.vel
        if keys[pygame.K_RIGHT] and dogup.x < 1000 - dogup.width - dogup.vel:
            dogup.x += dogup.vel
        if keys[pygame.K_DOWN] and dogup.y < 980 - dogup.height - dogup.vel:
            dogup.y += dogup.vel
        if keys[pygame.K_UP] and dogup.y > dogup.vel:
            dogup.y -= dogup.vel
    """ Controls for Dog Going Down are W, A, S, D """
    if(dogdown.isalive):
        if keys[pygame.K_q]:
            run = False
        if keys[pygame.K_a] and dogdown.x > dogdown.vel:
            dogdown.x -= dogdown.vel
        if keys[pygame.K_d] and dogdown.x < 1000 - dogdown.width - dogdown.vel:
            dogdown.x += dogdown.vel
        if keys[pygame.K_s] and dogdown.y < 980 - dogdown.height - dogdown.vel:
            dogdown.y += dogdown.vel
        if keys[pygame.K_w] and dogdown.y > dogdown.vel:
            dogdown.y -= dogdown.vel

    box1x = heart1.hitbox[0]
    box2x = heart2.hitbox[0]
    box3x = heart3.hitbox[0]
    box4x = heart4.hitbox[0]
    box5x = heart5.hitbox[0]
    box6x = heart6.hitbox[0]
    box7x = heart7.hitbox[0]
    box8x = heart8.hitbox[0]
    box1y = heart1.hitbox[1]
    box2y = heart2.hitbox[1]
    box3y = heart3.hitbox[1]
    box4y = heart4.hitbox[1]
    box5y = heart5.hitbox[1]
    box6y = heart6.hitbox[1]
    box7y = heart7.hitbox[1]
    box8y = heart8.hitbox[1]
    box1w = heart1.hitbox[2]
    box2w = heart2.hitbox[2]
    box3w = heart3.hitbox[2]
    box4w = heart4.hitbox[2]
    box5w = heart5.hitbox[2]
    box6w = heart6.hitbox[2]
    box7w = heart7.hitbox[2]
    box8w = heart8.hitbox[2]
    box1h = heart1.hitbox[3]
    box2h = heart2.hitbox[3]
    box3h = heart3.hitbox[3]
    box4h = heart4.hitbox[3]
    box5h = heart5.hitbox[3]
    box6h = heart6.hitbox[3]
    box7h = heart7.hitbox[3]
    box8h = heart8.hitbox[3]

    bm1x = bluemonster1.hitbox[0]
    bm2x = bluemonster2.hitbox[0]
    bm3x = bluemonster3.hitbox[0]
    bm1y = bluemonster1.hitbox[1]
    bm2y = bluemonster2.hitbox[1]
    bm3y = bluemonster3.hitbox[1]
    bm1w = bluemonster1.hitbox[2]
    bm2w = bluemonster2.hitbox[2]
    bm3w = bluemonster3.hitbox[2]
    bm1h = bluemonster1.hitbox[3]
    bm2h = bluemonster2.hitbox[3]
    bm3h = bluemonster3.hitbox[3]
    ro1x = rock1.hitbox[0]
    ro2x = rock2.hitbox[0]
    ro3x = rock3.hitbox[0]
    ro1y = rock1.hitbox[1]
    ro2y = rock2.hitbox[1]
    ro3y = rock3.hitbox[1]
    ro1w = rock1.hitbox[2]
    ro2w = rock2.hitbox[2]
    ro3w = rock3.hitbox[2]
    ro1h = rock1.hitbox[3]
    ro2h = rock2.hitbox[3]
    ro3h = rock3.hitbox[3]
    bh1x = blackhole1.hitbox[0]
    bh2x = blackhole2.hitbox[0]
    bh3x = blackhole3.hitbox[0]
    bh1y = blackhole1.hitbox[1]
    bh2y = blackhole2.hitbox[1]
    bh3y = blackhole3.hitbox[1]
    bh1w = blackhole1.hitbox[2]
    bh2w = blackhole2.hitbox[2]
    bh3w = blackhole3.hitbox[2]
    bh1h = blackhole1.hitbox[3]
    bh2h = blackhole2.hitbox[3]
    bh3h = blackhole3.hitbox[3]
    bo1x = bone1.hitbox[0]
    bo2x = bone2.hitbox[0]
    bo1y = bone1.hitbox[1]
    bo2y = bone2.hitbox[1]
    bo1w = bone1.hitbox[2]
    bo2w = bone2.hitbox[2]
    bo1h = bone1.hitbox[3]
    bo2h = bone2.hitbox[3]
    duh = dogup.height
    duw = dogup.width
    ddh = dogdown.height
    ddw = dogdown.width

    """ If Dog Up is active, hitbox interaction with the moving obstacles """
    if dogup.isalive:
        """ Y coordinate of the player between the hitbox range"""
        """And X coordinate of the player between the hitbox range """
        if dogup.y > box1y - duh and dogup.y < box1y + box1h:
            if dogup.x > box1x - duw and dogup.x < box1x + box1w:
                dogup.obstaclehit()
        if dogup.y > box2y - duh and dogup.y < box2y + box2h:
            if dogup.x > box2x - duw and dogup.x < box2x + box2w:
                dogup.obstaclehit()
        if dogup.y > box3y - duh and dogup.y < box3y + box3h:
            if dogup.x > box3x - duw and dogup.x < box3x + box3w:
                dogup.obstaclehit()
        if dogup.y > box4y - duh and dogup.y < box4y + box4h:
            if dogup.x > box4x - duw and dogup.x < box4x + box4w:
                dogup.obstaclehit()
        if dogup.y > box5y - duh and dogup.y < box5y + box5h:
            if dogup.x > box5x - duw and dogup.x < box5x + box5w:
                dogup.obstaclehit()
        if dogup.y > box6y - duh and dogup.y < box6y + box6h:
            if dogup.x > box6x - duw and dogup.x < box6x + box6w:
                dogup.obstaclehit()
        if dogup.y > box7y - duh and dogup.y < box7y + box7h:
            if dogup.x > box7x - duw and dogup.x < box7x + box7w:
                dogup.obstaclehit()
        if dogup.y > box8y - duh and dogup.y < box8y + box8h:
            if dogup.x > box8x - duw and dogup.x < box8x + box8w:
                dogup.obstaclehit()

        """ Hit Box Collision of Dog Up with Fixed Obstacles """

        if dogup.y > bm1y - duh and dogup.y < bm1y + bm1h:
            if dogup.x > bm1x - duw + 20 and dogup.x < bm1x + bm1w - 20:
                dogup.obstaclehit()
        if dogup.y > bm2y - duh and dogup.y < bm2y + bm2h:
            if dogup.x > bm2x - duw + 20 and dogup.x < bm2x + bm2w - 20:
                dogup.obstaclehit()
        if dogup.y > bm3y - duh and dogup.y < bm3y + bm3h:
            if dogup.x > bm3x - duw + 20 and dogup.x < bm3x + bm3w - 20:
                dogup.obstaclehit()

        if dogup.y > ro1y + 30 - duh and dogup.y < ro1y + ro1h:
            if dogup.x > ro1x - duw + 20 and dogup.x < ro1x + ro1w - 20:
                dogup.obstaclehit()
        if dogup.y > ro2y + 30 - duh and dogup.y < ro2y + ro2h:
            if dogup.x > ro2x - duw + 20 and dogup.x < ro2x + ro2w - 20:
                dogup.obstaclehit()
        if dogup.y > ro3y + 30 - duh and dogup.y < ro3y + ro3h:
            if dogup.x > ro3x - duw + 20 and dogup.x < ro3x + ro3w - 20:
                dogup.obstaclehit()

        if dogup.y > bh1y - duh and dogup.y < bh1y + bh1h:
            if dogup.x > bh1x - duw + 20 and dogup.x < bh1x + bh1w - 20:
                dogup.obstaclehit()
        if dogup.y > bh2y - duh and dogup.y < bh2y + bh2h:
            if dogup.x > bh2x - duw + 20 and dogup.x < bh2x + bh2w - 20:
                dogup.obstaclehit()
        if dogup.y > bh3y - duh and dogup.y < bh3y + bh3h:
            if dogup.x > bh3x - duw + 20 and dogup.x < bh3x + bh3w - 20:
                dogup.obstaclehit()
        if dogup.y > bo1y - duh and dogup.y < bo1y + bo1h:
            if dogup.x > bo1x - duw + 20 and dogup.x < bo1x + bo1w - 20:
                dogup.bonetouch()
    """ Hitbox Collision if Dog Down is active """
    if(dogdown.isalive):
        if dogdown.y > box1y - ddh and dogdown.y < box1y + box1h:
            if dogdown.x > box1x - ddw and dogdown.x < box1x + box1w:
                dogdown.obstaclehit()
        if dogdown.y > box2y - ddh and dogdown.y < box2y + box2h:
            if dogdown.x > box2x - ddw and dogdown.x < box2x + box2w:
                dogdown.obstaclehit()
        if dogdown.y > box3y - ddh and dogdown.y < box3y + box3h:
            if dogdown.x > box3x - ddw and dogdown.x < box3x + box3w:
                dogdown.obstaclehit()
        if dogdown.y > box4y - ddh and dogdown.y < box4y + box4h:
            if dogdown.x > box4x - ddw and dogdown.x < box4x + box4w:
                dogdown.obstaclehit()
        if dogdown.y > box5y - ddh and dogdown.y < box5y + box5h:
            if dogdown.x > box5x - ddw and dogdown.x < box5x + box5w:
                dogdown.obstaclehit()
        if dogdown.y > box6y - ddh and dogdown.y < box6y + box6h:
            if dogdown.x > box6x - ddw and dogdown.x < box6x + box6w:
                dogdown.obstaclehit()
        if dogdown.y > box7y - ddh and dogdown.y < box7y + box7h:
            if dogdown.x > box7x - ddw and dogdown.x < box7x + box7w:
                dogdown.obstaclehit()
        if dogdown.y > box8y - ddh and dogdown.y < box8y + box8h:
            if dogdown.x > box8x - ddw and dogdown.x < box8x + box8w:
                dogdown.obstaclehit()

        """ Hitbox Collision of Dog Down with Fixed Obstacles """
        if dogdown.y > bm1y - ddh and dogdown.y < bm1y + bm1h:
            if dogdown.x > bm1x - ddw + 20 and dogdown.x < bm1x + bm1w - 20:
                dogdown.obstaclehit()
        if dogdown.y > bm2y - ddh and dogdown.y < bm2y + bm2h:
            if dogdown.x > bm2x - ddw + 20 and dogdown.x < bm2x + bm2w - 20:
                dogdown.obstaclehit()
        if dogdown.y > bm3y - ddh and dogdown.y < bm3y + bm3h:
            if dogdown.x > bm3x - ddw + 20 and dogdown.x < bm3x + bm3w - 20:
                dogdown.obstaclehit()

        if dogdown.y > ro1y + 30 - ddh and dogdown.y < ro1y + ro1h:
            if dogdown.x > ro1x - ddw + 20 and dogdown.x < ro1x + ro1w - 20:
                dogdown.obstaclehit()
        if dogdown.y > ro2y + 30 - ddh and dogdown.y < ro2y + ro2h:
            if dogdown.x > ro2x - ddw + 20 and dogdown.x < ro2x + ro2w - 20:
                dogdown.obstaclehit()
        if dogdown.y > ro3y + 30 - ddh and dogdown.y < ro3y + ro3h:
            if dogdown.x > ro3x - ddw + 20 and dogdown.x < ro3x + ro3w - 20:
                dogdown.obstaclehit()

        if dogdown.y > bh1y - ddh and dogdown.y < bh1y + bh1h:
            if dogdown.x > bh1x - ddw + 20 and dogdown.x < bh1x + bh1w - 20:
                dogdown.obstaclehit()
        if dogdown.y > bh2y - ddh and dogdown.y < bh2y + bh2h:
            if dogdown.x > bh2x - ddw + 20 and dogdown.x < bh2x + bh2w - 20:
                dogdown.obstaclehit()
        if dogdown.y > bh3y - ddh and dogdown.y < bh3y + bh3h:
            if dogdown.x > bh3x - ddw + 20 and dogdown.x < bh3x + bh3w - 20:
                dogdown.obstaclehit()

        if dogdown.y > bo2y - ddh and dogdown.y < bo2y + bo2h:
            if dogdown.x > bo2x - ddw + 20 and dogdown.x < bo2x + bo2w - 20:
                dogdown.bonetouch()

    """ Movement of the moving obstacles """
    """ Following code allows the 'beating' heart, movement of the heart"""
    """ And switching directions when the end of the screen is reached """
    if(heart1.direction):
        heart1.x += heart1.vel
    else:
        heart1.x -= heart1.vel
    if(heart1.x > 930):
        heart1.direction = False
    if(heart1.x < 10):
        heart1.direction = True

    if(heart1.count <= 10):
        heart1.count += 1
    else:
        heart1.count += 1
    if(heart1.count == 20):
        heart1.count = 0

    if(heart2.direction):
        heart2.x += heart2.vel
    else:
        heart2.x -= heart2.vel
    if(heart2.x > 930):
        heart2.direction = False
    if(heart2.x < 10):
        heart2.direction = True

    if(heart2.count <= 10):
        heart2.count += 1
    else:
        heart2.count += 1
    if(heart2.count == 20):
        heart2.count = 0

    if(heart3.direction):
        heart3.x += heart3.vel
    else:
        heart3.x -= heart3.vel
    if(heart3.x > 930):
        heart3.direction = False
    if(heart3.x < 10):
        heart3.direction = True

    if(heart3.count <= 10):
        heart3.count += 1
    else:
        heart3.count += 1
    if(heart3.count == 20):
        heart3.count = 0

    if(heart4.direction):
        heart4.x += heart4.vel
    else:
        heart4.x -= heart4.vel
    if(heart4.x > 930):
        heart4.direction = False
    if(heart4.x < 10):
        heart4.direction = True

    if(heart4.count <= 10):
        heart4.count += 1
    else:
        heart4.count += 1
    if(heart4.count == 20):
        heart4.count = 0

    if(heart5.direction):
        heart5.x += heart5.vel
    else:
        heart5.x -= heart5.vel
    if(heart5.x > 930):
        heart5.direction = False
    if(heart5.x < 10):
        heart5.direction = True

    if(heart5.count <= 10):
        heart5.count += 1
    else:
        heart5.count += 1
    if(heart5.count == 20):
        heart5.count = 0

    if(heart6.direction):
        heart6.x += heart6.vel
    else:
        heart6.x -= heart6.vel
    if(heart6.x > 930):
        heart6.direction = False
    if(heart6.x < 10):
        heart6.direction = True

    if(heart6.count <= 10):
        heart6.count += 1
    else:
        heart6.count += 1
    if(heart6.count == 20):
        heart6.count = 0

    if(heart7.direction):
        heart7.x += heart7.vel
    else:
        heart7.x -= heart7.vel
    if(heart7.x > 930):
        heart7.direction = False
    if(heart7.x < 10):
        heart7.direction = True

    if(heart7.count <= 10):
        heart7.count += 1
    else:
        heart7.count += 1
    if(heart7.count == 20):
        heart7.count = 0

    if(heart8.direction):
        heart8.x += heart8.vel
    else:
        heart8.x -= heart8.vel
    if(heart8.x > 930):
        heart8.direction = False
    if(heart8.x < 10):
        heart8.direction = True

    if(heart8.count <= 10):
        heart8.count += 1
    else:
        heart8.count += 1
    if(heart8.count == 20):
        heart8.count = 0

    """" Scoring When Crossing Obstacles """
    if dogup.isalive:
        if dogup.y < heart8.y and dogup.first == 0:
            dogup.first = 1
        if dogup.y < rock3.y and dogup.second == 0:
            dogup.second = 1
        if dogup.y < heart5.y and dogup.third == 0:
            dogup.third = 1
        if dogup.y < bluemonster3.y and dogup.fourth == 0:
            dogup.fourth = 1
        if dogup.y < heart4.y and dogup.fifth == 0:
            dogup.fifth = 1
        if dogup.y < bluemonster2.y and dogup.sixth == 0:
            dogup.sixth = 1
        if dogup.y < heart3.y and dogup.seventh == 0:
            dogup.seventh = 1
        if dogup.y < rock1.y and dogup.eighth == 0:
            dogup.eighth = 1
        if dogup.y < heart2.y and dogup.ninth == 0:
            dogup.ninth = 1
        if dogup.y < bluemonster1.y and dogup.tenth == 0:
            dogup.tenth = 1
        if dogup.y < heart1.y and dogup.eleventh == 0:
            dogup.eleventh = 1

        """ 0 = player did not cross"""
        """ 1 = player crossed but wasn't given points"""
        """ 2 = player crossed and was given points """
        if dogup.first == 1:
            dogup.first = 2
            dogup.score += 10
        if dogup.second == 1:
            dogup.second = 2
            dogup.score += 5
        if dogup.third == 1:
            dogup.third = 2
            dogup.score += 10
        if dogup.fourth == 1:
            dogup.fourth = 2
            dogup.score += 10
        if dogup.fifth == 1:
            dogup.fifth = 2
            dogup.score += 20
        if dogup.sixth == 1:
            dogup.sixth = 2
            dogup.score += 15
        if dogup.seventh == 1:
            dogup.seventh = 2
            dogup.score += 20
        if dogup.eighth == 1:
            dogup.eighth = 2
            dogup.score += 5
        if dogup.ninth == 1:
            dogup.ninth = 2
            dogup.score += 10
        if dogup.tenth == 1:
            dogup.tenth = 2
            dogup.score += 10
        if dogup.eleventh == 1:
            dogup.eleventh = 2
            dogup.score += 10

    else:
        if dogdown.y > heart8.y + 30 and dogdown.first == 0:
            dogdown.first = 1
        if dogdown.y > rock3.y + 30 and dogdown.second == 0:
            dogdown.second = 1
        if dogdown.y > heart5.y + 30 and dogdown.third == 0:
            dogdown.third = 1
        if dogdown.y > bluemonster3.y + 30 and dogdown.fourth == 0:
            dogdown.fourth = 1
        if dogdown.y > heart4.y + 30 and dogdown.fifth == 0:
            dogdown.fifth = 1
        if dogdown.y > bluemonster2.y + 30 and dogdown.sixth == 0:
            dogdown.sixth = 1
        if dogdown.y > heart3.y + 30 and dogdown.seventh == 0:
            dogdown.seventh = 1
        if dogdown.y > rock1.y + 30 and dogdown.eighth == 0:
            dogdown.eighth = 1
        if dogdown.y > heart2.y + 30 and dogdown.ninth == 0:
            dogdown.ninth = 1
        if dogdown.y > bluemonster1.y + 30 and dogdown.tenth == 0:
            dogdown.tenth = 1
        if dogdown.y > heart1.y + 30 and dogdown.eleventh == 0:
            dogdown.eleventh = 1

        if dogdown.first == 1:
            dogdown.first = 2
            dogdown.score += 10
        if dogdown.second == 1:
            dogdown.second = 2
            dogdown.score += 5
        if dogdown.third == 1:
            dogdown.third = 2
            dogdown.score += 10
        if dogdown.fourth == 1:
            dogdown.fourth = 2
            dogdown.score += 10
        if dogdown.fifth == 1:
            dogdown.fifth = 2
            dogdown.score += 20
        if dogdown.sixth == 1:
            dogdown.sixth = 2
            dogdown.score += 15
        if dogdown.seventh == 1:
            dogdown.seventh = 2
            dogdown.score += 20
        if dogdown.eighth == 1:
            dogdown.eighth = 2
            dogdown.score += 5
        if dogdown.ninth == 1:
            dogdown.ninth = 2
            dogdown.score += 10
        if dogdown.tenth == 1:
            dogdown.tenth = 2
            dogdown.score += 10
        if dogdown.eleventh == 1:
            dogdown.eleventh = 2
            dogdown.score += 10
    """   """
    if dogup.isalive:
        if dogup.timescore > 0:
            dogup.timescore -= 5
    else:
        if dogdown.timescore > 0:
            dogdown.timescore -= 5

    if not dogup.totalalive and not dogdown.totalalive:
        largefont = pygame.font.Font(config.fontstyle, config.largefontsize)
        if dogup.score > dogdown.score:
            lastScore = largefont.render(config.dogupwins, 1, (0, 200, 50))
            score = largefont.render(
                config.score + str(dogup.score), 1, (0, 200, 50))
        elif dogup.score < dogdown.score:
            lastScore = largefont.render(config.dogdownwins, 1, (0, 200, 50))
            score = largefont.render(
                config.score + str(dogdown.score), 1, (0, 200, 50))
        else:
            lastScore = largefont.render(config.tiemessage, 1, (0, 200, 50))
            score = largefont.render(
                config.score + str(dogup.score), 1, (0, 200, 50))

        win.blit(lastScore, (300, 400))
        win.blit(score, (350, 500))

        pygame.display.update()
        pygame.time.wait(5000)
        run = False
    drawGameWindow()
